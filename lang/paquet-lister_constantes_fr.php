<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'lister_constantes_description' => 'Ce plugin permet de lister constantes PHP de SPIP',
	'lister_constantes_nom' => 'Lister les constantes PHP de SPIP',
	'lister_constantes_slogan' => 'Le support de votre application.',
);
