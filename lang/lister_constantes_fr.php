<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'description_lister_constantes' => 'On va lister sur cette page toutes les constantes PHP de SPIP.',
	'des_constantes' => '@nb@ constantes',

	// I
	'info_constante' => 'Constante',
	'info_valeur' => 'Valeur',

	// T
	'titre_lister_constantes' => 'Lister les constantes PHP de SPIP',
	'titre_page' => 'Lister les constantes PHP de SPIP',
	'titre_page_constantes' => 'Lister les constantes',

	// U
	'une_constante' => 'Une constante',

);
